import { Movie } from './movie.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Injectable()
export class MovieService {
  moviesChange = new Subject<Movie[]>();
  moviesFetching = new Subject<boolean>();
  movieAdding = new Subject<boolean>();
  movieDeleting = new Subject<boolean>();

  private moviesArray: Movie[] = [];

  constructor(private http: HttpClient) {}

  getMovies() {
    return this.moviesArray.slice();
  }

  fetchMovies() {
    this.moviesFetching.next(true);
    this.http.get<{[id: string]: Movie}>('https://kamilya-61357-default-rtdb.firebaseio.com/movies.json')
      .pipe(map(result => {
        if(result === null) {
          return [];
        }
        return Object.keys(result).map(id => {
          const moviesData = result[id];
          return new Movie(id, moviesData.name);
        })
      }))
      .subscribe(movies => {
        this.moviesArray = movies;
        this.moviesChange.next(this.moviesArray.slice());
        this.moviesFetching.next(false);
      }, () => {
        this.moviesFetching.next(false);
      });
  }

  addNewMovie(name: string) {
    this.movieAdding.next(true);
    const body = {name};
    this.http.post('https://kamilya-61357-default-rtdb.firebaseio.com/movies.json', body)
      .subscribe(() => {
        this.movieAdding.next(false);
      }, () => {
        this.movieAdding.next(false);
      });
  }

  deleteMovieFromList(id: string) {
    this.movieDeleting.next(true);
    this.http.delete(`https://kamilya-61357-default-rtdb.firebaseio.com/movies/${id}.json`)
      .subscribe(() => {
        this.movieDeleting.next(false);
      }, () => {
        this.movieDeleting.next(false);
      });
  }
}
