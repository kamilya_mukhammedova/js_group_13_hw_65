import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MovieService } from '../shared/movie.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-add-movie',
  templateUrl: './add-movie.component.html',
  styleUrls: ['./add-movie.component.css']
})
export class AddMovieComponent implements OnInit, OnDestroy {
  @ViewChild('nameMovieInput') nameMovieInput!: ElementRef;
  adding = false;
  movieAddingSubscription!: Subscription;

  constructor(private movieService: MovieService) { }

  ngOnInit() {
    this.movieAddingSubscription = this.movieService.movieAdding.subscribe((isAdding: boolean) => {
      this.adding = isAdding;
    });
  }

  addMovie() {
    const movieName = this.nameMovieInput.nativeElement.value;
    if(movieName !== '') {
      this.movieService.addNewMovie(movieName);
      this.nameMovieInput.nativeElement.value = '';
    } else {
      alert('You need to enter the movie\'s name.');
    }
  }

  ngOnDestroy() {
    this.movieAddingSubscription.unsubscribe();
  }
}
