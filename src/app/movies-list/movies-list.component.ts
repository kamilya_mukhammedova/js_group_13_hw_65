import { Component, OnDestroy, OnInit } from '@angular/core';
import { Movie } from '../shared/movie.model';
import { MovieService } from '../shared/movie.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-movies-list',
  templateUrl: './movies-list.component.html',
  styleUrls: ['./movies-list.component.css']
})
export class MoviesListComponent implements OnInit, OnDestroy {
  movies!: Movie[];
  loadingList = false;
  deletingMovie = false;
  moviesChangeSubscription!: Subscription;
  moviesFetchingSubscription! : Subscription;
  movieDeletingSubscription! : Subscription;

  constructor(private movieService: MovieService) { }

  ngOnInit() {
    this.movies = this.movieService.getMovies();
    this.moviesChangeSubscription = this.movieService.moviesChange.subscribe((movies: Movie[]) => {
      this.movies = movies;
    });
    this.moviesFetchingSubscription = this.movieService.moviesFetching.subscribe((isFetching: boolean) => {
      this.loadingList = isFetching;
    });
    this.movieDeletingSubscription = this.movieService.movieDeleting.subscribe((isDeleting: boolean) => {
      this.deletingMovie = isDeleting;
    });
    this.movieService.fetchMovies();
  }

  deleteMovie(movieId: string, index: number) {
    this.movieService.deleteMovieFromList(movieId);
    this.movies[index].isDeleted = true;
  }

  ngOnDestroy() {
    this.moviesChangeSubscription.unsubscribe();
    this.moviesFetchingSubscription.unsubscribe();
    this.movieDeletingSubscription.unsubscribe();
  }
}

